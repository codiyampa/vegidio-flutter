package io.vinicius.appodeal_flutter

import com.appodeal.ads.NativeAd

object NativeAdSingleton {
    var loadedAds: MutableMap<Long, NativeAd> = mapOf<Long, NativeAd>() as MutableMap<Long, NativeAd>
}