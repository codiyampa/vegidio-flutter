package io.vinicius.appodeal_flutter

import com.appodeal.ads.Appodeal
import com.appodeal.ads.NativeAd
import com.appodeal.ads.NativeCallbacks
import io.flutter.plugin.common.MethodChannel

fun nativeCallback(channel: MethodChannel): NativeCallbacks {
    return object : NativeCallbacks {
        override fun onNativeLoaded() {
            channel.invokeMethod("onNativeLoaded", null)
        }

        override fun onNativeFailedToLoad() {
            channel.invokeMethod("onNativeFailedToLoad", null)
        }

        override fun onNativeShown(p0: NativeAd?) {
            channel.invokeMethod("onNativeShown", null)
        }

        override fun onNativeShowFailed(p0: NativeAd?) {
            // Not implemented for the sake of consistency with iOS
        }

        override fun onNativeClicked(p0: NativeAd?) {
            channel.invokeMethod("onNativeClicked", null)
        }

        override fun onNativeExpired() {
            channel.invokeMethod("onNativeExpired", null)
        }
    }
}