package io.vinicius.appodeal_flutter;

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.appodeal.ads.Appodeal
import com.appodeal.ads.NativeAd
import com.appodeal.ads.native_ad.views.NativeAdViewContentStream
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import java.util.*


class AppodealNativeView(
    context: Context,
    activity: Activity,
    messenger: BinaryMessenger,
    id: Int,
    arguments: Map<Any, Any>
) : PlatformView {

    private lateinit var nativeAdView: View

    private var context: Context
    private var activity: Activity
    private var placementName: String?
    private var viewId: Int

    private val methodChannel: MethodChannel = MethodChannel(messenger, "plugins.io.vinicius.appodeal/native_$id")
    private var nativeAd: NativeAd? = null
    private var placeholder: LinearLayout? = null

    val timer : Timer = Timer()
    val task = object : TimerTask() {
        override fun run() {
            println("Android native ad $id: check for ad.")
            if (Appodeal.getAvailableNativeAdsCount() > 0) {
                activity.runOnUiThread(object : TimerTask() {
                    override fun run() {
                        loadAdView()
                    }
                })
            } else {
                Appodeal.cache(activity, Appodeal.NATIVE)
            }
        }
    }

    init {
        this.context = context
        this.activity = activity
        this.placementName = arguments["placementName"] as? String;
        this.viewId = id;

        Appodeal.cache(activity, Appodeal.NATIVE)

        println("Android native ad $viewId: available native count before timer start: " + Appodeal.getAvailableNativeAdsCount())
        if (Appodeal.getAvailableNativeAdsCount() > 0) {
            loadAdView();
        } else {
            injectPlaceholder()
            timer.scheduleAtFixedRate(task, 250, 250)
        }
    }

    private fun loadAdView() {
        try {
            println("Android native ad $viewId: ad should be available. there are: " + Appodeal.getAvailableNativeAdsCount())
            if (Appodeal.getAvailableNativeAdsCount() > 0) {
                val nativeAdList = Appodeal.getNativeAds(1);
                println("Android native ad $viewId: available native count after get: " + Appodeal.getAvailableNativeAdsCount())

                nativeAd = nativeAdList[0];
                if (placeholder != null) {
                    val placeholderAdView = NativeAdViewContentStream(this.context, this.nativeAd, this.placementName);
                    placeholder!!.addView(placeholderAdView)
                } else {
                    nativeAdView = NativeAdViewContentStream(this.context, this.nativeAd, this.placementName);
                }
                methodChannel.invokeMethod("onNativeAdViewLoaded", null)
                timer.cancel()
            }
        } catch (e: Exception) {
            println("Android native ad $viewId: loadAdView Exception: " + e.message)
            if (placeholder == null) {
                injectPlaceholder()
            }
        }
    }

    private fun injectPlaceholder() {
        placeholder = LinearLayout(this.context)
        placeholder!!.id = View.generateViewId()
        placeholder!!.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        nativeAdView = placeholder as LinearLayout;
    }

    override fun getView(): View {
        return nativeAdView
    }

    override fun dispose() {
        // nativeAd?.destroy()
        methodChannel.setMethodCallHandler(null)
    }
}