import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class AppodealNative extends StatefulWidget {
  final String placementName;
  final Function onNativeAdViewLoaded;

  const AppodealNative({
    Key key,
    this.placementName,
    this.onNativeAdViewLoaded,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _AppodealNativeState(placementName, onNativeAdViewLoaded);
}

class _AppodealNativeState extends State<AppodealNative> {
  final Map<String, dynamic> _args = {};
  Function onNativeAdViewLoaded;

  _AppodealNativeState(String placementName, Function onNativeAdViewLoaded) {
    this._args["placementName"] = placementName;
    this.onNativeAdViewLoaded = onNativeAdViewLoaded;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 350,
      child: Platform.isIOS
          ? /* UiKitView(
              viewType: 'plugins.io.vinicius.appodeal/native',
              onPlatformViewCreated: _onPlatformViewCreated,
              creationParams: this._args,
              creationParamsCodec: const StandardMessageCodec(),
            ) */
          SizedBox()
          : AndroidView(
              viewType: 'plugins.io.vinicius.appodeal/native',
              onPlatformViewCreated: _onPlatformViewCreated,
              creationParams: this._args,
              creationParamsCodec: const StandardMessageCodec(),
            ),
    );
  }

  void _onPlatformViewCreated(int id) {
    final NativeAdViewController controller = NativeAdViewController._(id);
    controller._channel.setMethodCallHandler(_messageFromNative);
  }

  Future<void> _messageFromNative(MethodCall call) async {
    switch (call.method) {
      case "onNativeAdViewLoaded":
        onNativeAdViewLoaded();
        break;
    }
  }
}

/// Controller MethodChannel Flutter <-> Android and iOS.
class NativeAdViewController {
  NativeAdViewController._(int id) : _channel = _createChannel(id);

  final MethodChannel _channel;

  static MethodChannel _createChannel(int id) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return MethodChannel('plugins.io.vinicius.appodeal/native_$id');
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      return MethodChannel('plugins.io.vinicius.appodeal/native_$id');
    } else {
      return throw MissingPluginException();
    }
  }
}
